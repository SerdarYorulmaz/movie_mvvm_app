package com.example.movie_mvvm_app.data.api

import com.example.movie_mvvm_app.data.vo.MovieDetails
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

/**
    Kullanılacak  url veri icin gerekli:

    https://api.themoviedb.org/3/movie/popular?api_key=6e63c2317fbe963d76c3bdc2b785f6d1&page=1  -->Tv dizisi icin
    https://api.themoviedb.org/3/movie/299534?api_key=6e63c2317fbe963d76c3bdc2b785f6d1
    https://api.themoviedb.org/3/  -->BASE_URL retrofit icin

    import io.reactivex.Single //rx java kullanıyruz

 */

interface TheMovieDBInterface {

    @GET("movie/{movie_id}")
    fun getMovieDetails(@Path("movie_id") id:Int):Single<MovieDetails>
}