package com.example.movie_mvvm_app.data.repository


enum class Status{
    RUNNING,
    SUCCESS,
    FAILED
}
 /** api_key: 6e63c2317fbe963d76c3bdc2b785f6d1 **/
class NetworkState(val status: Status,val msn:String) {

    companion object{

        val LOADED:NetworkState
        val LOADING:NetworkState
        val ERROR:NetworkState

        init {
            LOADED= NetworkState(Status.SUCCESS,"Başarılı")
            LOADING= NetworkState(Status.RUNNING,"Yükleniyor")
            ERROR= NetworkState(Status.FAILED,"Bir şeyler yanlış gitti")
        }
    }

}