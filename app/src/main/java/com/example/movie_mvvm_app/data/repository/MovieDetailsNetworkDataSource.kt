package com.example.movie_mvvm_app.data.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.movie_mvvm_app.data.api.TheMovieDBInterface
import com.example.movie_mvvm_app.data.vo.MovieDetails
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/** compasiteDisposable -> tek kullanımlık **/
class MovieDetailsNetworkDataSource(private val apiService:TheMovieDBInterface,private val compasiteDisposable:CompositeDisposable) {

    private val _networkState= MutableLiveData<NetworkState>()
    val networkState:LiveData<NetworkState>
    get() = _networkState

    private  val _downloadedMovieDetailsResponse=MutableLiveData<MovieDetails>()

    val downloadMovieResponse:LiveData<MovieDetails> //model aldik

    get() = _downloadedMovieDetailsResponse

    fun fetchMovieDetails(movieId:Int){

        _networkState.postValue(NetworkState.LOADING)

        try {

            compasiteDisposable.add(

                apiService.getMovieDetails(movieId)
                    .subscribeOn(Schedulers.io())
                    .subscribe(
                        {
                            _downloadedMovieDetailsResponse.postValue(it)
                            _networkState.postValue(NetworkState.LOADED)
                        },
                        {
                            _networkState.postValue(NetworkState.ERROR)
                            Log.e("MovieDetailsDataSource",it.message)
                        }
                    )

            )


        }catch (e:Exception){
            Log.e("HATA","Verilen Hata:${e.message}")
        }

    }





}